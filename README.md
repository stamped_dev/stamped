# Stamped

How to run: - To test application with data already connected to user use debug mode to automatically sign in
as mocked user. - When running in release mode adding SHA-1 key to firebase might be required.
In order to acquire your SHA-1 key go to stamped/android and run command
`.\gradlew signingReport` or `gradlew signingReport` - in order to run application properly user must grant location permission - when mocking location pleas use only locations in Slovakia or czech republic since only these
two have their own stamps and flags added to DB - reasoning behind this was putting more time
into developing app rather than playing around with various assets - application might lack permission settings for ios (we didn't have a chance to test it on ios
device)

## Black Screen

If you are in debug mode and are stuck on a loading screen, please hot restart the app.

Developed by:
Michal Cikatricis 485669
Tuan Anh Nguyen 514066
