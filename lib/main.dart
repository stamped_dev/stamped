import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:stamped/common/service/ioc_container.dart';
import 'package:stamped/common/widget/app_wrapper.dart';
import 'firebase_options.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );

  IoCContainer.initialize();
  runApp(AppWrapper());
}
