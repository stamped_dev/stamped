import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class CachedImageBuilder extends StatelessWidget {
  final String _url;
  final BoxFit? fit;
  final double? width;
  final double? height;

  const CachedImageBuilder(
      {super.key,
      required url,
      this.fit,
      this.width,
      this.height})
      : _url = url;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: CachedNetworkImageProvider(_url),
          fit: fit ?? BoxFit.cover,
        ),
      ),
    );
  }
}
