import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:stamped/bottom_nav_bar/widget/bottom_nav_bar.dart';
import 'package:stamped/common/service/ioc_container.dart';
import 'package:stamped/common/widget/future_builder_handler.dart';
import 'package:stamped/pages/auth/login_page.dart';
import 'package:stamped/pages/create/notifiers/create_postcard_notifier.dart';
import 'package:stamped/service/auth_service.dart';

class AppWrapper extends StatelessWidget {
  // final Widget child;

  const AppWrapper({
    super.key,
    // required this.child,
  });

  @override
  Widget build(BuildContext context) {
    final authService = get<AuthService>();
    return ChangeNotifierProvider(
      create: (context) => CreatePostcardNotifier(),
      child: MaterialApp(
        title: 'Stamped!',
        theme: ThemeData(
          useMaterial3: true,
          scaffoldBackgroundColor: Color(0xFFEFEFEF),
        ),
        home: StreamBuilder(
          stream: authService.userStream,
          builder: (context, snapshot) {
            if (!kDebugMode) {
              if (snapshot.hasError) {
                return Center(child: Text('Error: ${snapshot.error!}'));
              }

              if (!snapshot.hasData) {
                return LoginPage();
              }
            }
            return FutureBuilderHandler(
              future: authService.setUser(),
              builder: (context, data) {
                return BottomNavBar();
              },
            );
          },
        ),
      ),
    );
  }
}
