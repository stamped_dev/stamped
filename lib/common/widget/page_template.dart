import 'package:flutter/material.dart';
import 'package:stamped/common/util/shared_ui_constants.dart';

class PageTemplate extends StatelessWidget {
  final String title;
  final Widget child;
  final List<Widget> actions;
  final double padding;

  const PageTemplate({
    super.key,
    required this.title,
    required this.child,
    this.actions = const [],
    this.padding = STANDARD_GAP,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
        centerTitle: true,
        scrolledUnderElevation: 0.0,
        actions: actions,
      ),
      body: Padding(
        padding: EdgeInsets.all(padding),
        child: child,
      ),
      resizeToAvoidBottomInset: false,
    );
  }
}
