import 'package:get_it/get_it.dart';
import 'package:stamped/service/auth_service.dart';
import 'package:stamped/service/media_service.dart';
import 'package:stamped/service/postcard_service.dart';
import 'package:stamped/service/user_service.dart';

final get = GetIt.instance;

class IoCContainer {
  IoCContainer._();

  static void initialize() {
    get.registerSingleton(MediaService());
    get.registerSingleton(UserService());
    get.registerSingleton(
      AuthService(
        get<UserService>(),
        get<MediaService>(),
      ),
    );
    get.registerSingleton(
      PostcardService(
        get<UserService>(),
        get<AuthService>(),
      ),
    );
  }
}
