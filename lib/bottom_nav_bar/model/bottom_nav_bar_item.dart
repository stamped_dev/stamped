import 'package:flutter/material.dart';

class BottomNavBarItem {
  final String label;
  final Widget icon;
  final Widget child;

  const BottomNavBarItem({
    required this.label,
    required this.icon,
    required this.child,
  });
}
