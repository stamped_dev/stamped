import 'package:flutter/material.dart';
import 'package:stamped/bottom_nav_bar/model/bottom_nav_bar_item.dart';
import 'package:stamped/pages/collections/collections_page.dart';
import 'package:stamped/pages/create/create_front/select_image.dart';
import 'package:stamped/pages/home/home_page.dart';
import 'package:stamped/pages/profile/profile_page.dart';

final List<BottomNavBarItem> _navItems = [
  BottomNavBarItem(
    label: 'Home',
    icon: Icon(Icons.home),
    child: HomePage(),
  ),
  BottomNavBarItem(
    label: 'Collection',
    icon: Icon(Icons.mail),
    child: CollectionsPage(),
  ),
  BottomNavBarItem(
    label: 'Create',
    icon: Icon(Icons.add_circle_outline),
    child: SelectImage(),
  ),
  BottomNavBarItem(
    label: 'Profile',
    icon: Icon(Icons.person),
    child: ProfilePage(),
  ),
];

class BottomNavBar extends StatefulWidget {
  const BottomNavBar({Key? key}) : super(key: key);

  @override
  State<BottomNavBar> createState() => _BottomNavBarState();
}

class _BottomNavBarState extends State<BottomNavBar> {
  int _currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _navItems[_currentIndex].child,
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentIndex,
        items: _navItems
            .map(
              (item) => BottomNavigationBarItem(
                label: item.label,
                icon: item.icon,
              ),
            )
            .toList(),
        onTap: _onItemTap,
        type: BottomNavigationBarType.fixed,
        selectedItemColor: Colors.black87,
        unselectedItemColor: Colors.grey[400],
        showSelectedLabels: false,
        showUnselectedLabels: false,
      ),
    );
  }

  void _onItemTap(int index) => setState(() => _currentIndex = index);
}
