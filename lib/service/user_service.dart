import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart' as auth;
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:rxdart/rxdart.dart';
import 'package:stamped/data/entities/user.dart';

class UserService {
  final _userCollection =
      FirebaseFirestore.instance.collection('users').withConverter(
    fromFirestore: (snapshot, options) {
      final json = snapshot.data() ?? {};
      json['id'] = snapshot.id;
      return User.fromJson(json);
    },
    toFirestore: (value, options) {
      final json = value.toJson();
      json.remove('id');
      return json;
    },
  );

  final _users = BehaviorSubject<List<User>>.seeded([]);

  UserService() {
    final userStream = _userCollection.snapshots().map((querySnapshot) =>
        querySnapshot.docs.map((docSnapshot) => docSnapshot.data()).toList());

    _users.addStream(userStream);
  }

  Stream<List<User>> get usersStream => _users.stream;

  Future<User> createFromEntity(User userEntity) async {
    try {
      final userSnapshot = (await _userCollection.add(userEntity)).get();
      final user = (await userSnapshot).data()!;
      return user;
    } catch (e) {
      throw Exception('Failed to create user');
    }
  }

  Future<User> createFromFirebase(auth.User firebaseUser) async {
    final user = User(
      id: '',
      googleAuthId: firebaseUser.uid,
      isAdmin: false,
      lastLocation: await getCurrentDeviceLocation(),
      profilePicture: firebaseUser.photoURL ?? '',
      userName: firebaseUser.displayName ?? firebaseUser.email ?? '',
    );

    return createFromEntity(user);
  }

  Future<User> updateUserLocation(String googleAuthId) async {
    print('Updating user location...');
    final user = await findByGoogleId(googleAuthId);
    try {
      await _userCollection.doc(user.id).update({
        'lastLocation': await getCurrentDeviceLocation(),
      });

      return await findByGoogleId(googleAuthId);
    } catch (e) {
      print(e);
      return user;
    }
  }

  Future<void> delete(String userId) {
    return _userCollection.doc(userId).delete();
  }

  Future<User> findByGoogleId(String userGoogleId) async {
    final userSnapshot = await _userCollection
        .where('googleAuthId', isEqualTo: userGoogleId)
        .get();
    final result = userSnapshot.docs;
    if (result.isEmpty) {
      throw Exception('User with ID $userGoogleId not found');
    }
    return result.first.data();
  }

  Future<String> getCurrentDeviceLocation() async {
    await _ensureLocationPermission();
    Position position = await Geolocator.getCurrentPosition(
      desiredAccuracy: LocationAccuracy.high,
    );

    List<Placemark> placemarks = await placemarkFromCoordinates(
      position.latitude,
      position.longitude,
    );

    if (placemarks.isEmpty || placemarks.first.country == null) {
      throw Exception('Location update failed.');
    }

    return _czechiaFix(placemarks.first.country!);
  }

  String _czechiaFix(String country) {
    if (country == 'Czechia') {
      return 'Czech Republic';
    }

    return country;
  }

  Future<void> _ensureLocationPermission() async {
    // if (!await Geolocator.isLocationServiceEnabled()) {
    //   throw Exception('Location services are disabled.');
    // }

    LocationPermission permission = await Geolocator.checkPermission();

    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
    }
  }

  void changeUserName({
    required String id,
    required String newName,
  }) async {
    try {
      _userCollection.doc(id).update(
        {
          'userName': newName,
        },
      );
    } catch (e) {
      print(e);
    }
  }

  void changeProfilePicture({
    required String id,
    required String pictureUrl,
  }) async {
    try {
      _userCollection.doc(id).update(
        {
          'profilePicture': pictureUrl,
        },
      );
    } catch (e) {
      print(e);
    }
  }
}
