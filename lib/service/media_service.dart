import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:country_picker/country_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:path/path.dart' as p;
import 'package:stamped/data/entities/country_entity.dart';
import 'package:stamped/data/enums/image_type.dart';

class MediaService {
  final _postcardImagesRef =
      FirebaseStorage.instance.ref().child('postcard_images');
  final _stampsRef = FirebaseStorage.instance.ref().child('stamps');
  final _profilePicturesRef =
      FirebaseStorage.instance.ref().child('profile_pictures');

  final _countryCollection =
      FirebaseFirestore.instance.collection('countries').withConverter(
    fromFirestore: (snapshot, options) {
      final json = snapshot.data() ?? {};
      json['id'] = snapshot.id;
      return CountryEntity.fromJson(json);
    },
    toFirestore: (value, options) {
      final json = value.toJson();
      json.remove('id');
      return json;
    },
  );

  String getFlagBitcode(String country) {
    return Country.tryParse(country)?.flagEmoji ?? '';
  }

  Future<Reference?> uploadImage(
    File file, {
    required ImageType imageType,
  }) async {
    late final Reference storageRef;

    switch (imageType) {
      case ImageType.postcard:
        storageRef = _postcardImagesRef;
        break;
      case ImageType.stamp:
        storageRef = _stampsRef;
        break;
      case ImageType.profilePicture:
        storageRef = _profilePicturesRef;
        break;
      default:
        break;
    }
    final fileName = _genUniqueFileName(file);
    final uploadTask = await storageRef.child(fileName).putFile(file);

    if (uploadTask.state != TaskState.success) {
      return null;
    }

    return storageRef.child(fileName);
  }

  String _genUniqueFileName(File file) {
    final time = DateTime.now().millisecondsSinceEpoch.toString();

    return '$time${p.basename(file.path)}';
  }

  Future<void> deleteImage() async {
    //TODO
  }

  Future<List<Future<String>>> getStampUrls(String country) async {
    final listResult = await _stampsRef.child(country).listAll();

    return listResult.items.map((e) => e.getDownloadURL()).toList();
  }
}
