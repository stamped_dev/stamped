import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:rxdart/rxdart.dart';
import 'package:stamped/data/entities/postcard_entity.dart';
import 'package:stamped/data/models/postcard.dart';
import 'package:stamped/service/auth_service.dart';
import 'package:stamped/service/user_service.dart';

class PostcardService {
  final UserService _userService;
  final AuthService _authService;
  final _postcardCollection =
      FirebaseFirestore.instance.collection('postcards').withConverter(
    fromFirestore: (snapshot, options) {
      final json = snapshot.data() ?? {};
      json['id'] = snapshot.id;
      return PostcardEntity.fromJson(json);
    },
    toFirestore: (value, options) {
      final json = value.toJson();
      json.remove('id');
      return json;
    },
  );

  PostcardService(this._userService, this._authService);

  Stream<List<Postcard>> get availablePostcardStream {
    final user = _authService.currentUser;
    final thirtyMinutesAgo = DateTime.now().subtract(Duration(minutes: 30));
    Timestamp timestamp = Timestamp.fromDate(thirtyMinutesAgo);
    final snapshots = _postcardCollection
        .where('receiverId', isEqualTo: '')
        .where('destinationLocation',
            isEqualTo: user.lastLocation)
        .where('timestamp', isGreaterThanOrEqualTo: timestamp)
        .orderBy('timestamp', descending: true)
        .limit(8)
        .snapshots();
    return _processSnapshot(snapshots);
  }

  Stream<List<Postcard>> get recentPostcardStream {
    final user = _authService.currentUser;
    final snapshots = _postcardCollection
        .where('receiverId', isEqualTo: user.id)
        .orderBy('timestamp', descending: true)
        .limit(8)
        .snapshots();
    return _processSnapshot(snapshots);
  }

  Stream<List<Postcard>> get postcardStream {
    final user = _authService.currentUser;
    final snapshots = _postcardCollection
        .where('receiverId', isEqualTo: user.id)
        .orderBy('timestamp', descending: true)
        .snapshots();
    return _processSnapshot(snapshots);
  }

  Stream<Set<String>> get stampStream {
    final user = _authService.currentUser;
    final snapshots = _postcardCollection
        .where('receiverId', isEqualTo: user.id)
        .orderBy('timestamp', descending: true)
        .snapshots();
    return _stampStreamWrapper(snapshots);
  }

  Stream<Set<String>> get recentStampStream {
    final user = _authService.currentUser;
    final snapshots = _postcardCollection
        .where('receiverId', isEqualTo: user.id)
        .orderBy('timestamp', descending: true)
        .limit(8)
        .snapshots();
    return _stampStreamWrapper(snapshots);
  }

  Future<void> updatePostcardReceiver(PostcardEntity entity) async {
    try {
      await _postcardCollection.doc(entity.id).set(entity);
    } catch (e) {
      print('Error updating postcard: $e');
      rethrow;
    }
  }

  Future<void> create(PostcardEntity postcardEntity) {
    return _postcardCollection.add(postcardEntity);
  }

  Future<void> delete(String postcardId) {
    return _postcardCollection.doc(postcardId).delete();
  }

  Future<PostcardEntity> findById(String postcardId) async {
    final postcardSnapshot = await _postcardCollection.doc(postcardId).get();
    if (!postcardSnapshot.exists) {
      throw Exception('Postcard with ID $postcardId not found');
    }
    return postcardSnapshot.data()!;
  }

  Stream<Set<String>> _stampStreamWrapper(
      Stream<QuerySnapshot<PostcardEntity>> snapshots) {
    final entities = _convertSnapshotToEntity(snapshots);
    return entities.map((List<PostcardEntity> postcards) {
      return postcards.map((postcard) => postcard.stampPath).toSet();
    });
  }

  Stream<List<Postcard>> _getPostcardStream(
      Stream<List<PostcardEntity>> postcardStream) {
    final userStream = _userService.usersStream;
    return CombineLatestStream.combine2(
      userStream,
      postcardStream,
      (users, postcards) => postcards.map((p) {
        final sender = users.where((u) => u.id == p.senderId).first;
        final receiver = p.receiverId == ''
            ? null
            : users.where((u) => u.id == p.receiverId).first;
        return Postcard(
          id: p.id,
          imagePath: p.imagePath,
          stampPath: p.stampPath,
          originLocation: p.originLocation,
          destinationLocation: p.destinationLocation,
          text: p.text,
          sender: sender,
          receiver: receiver,
          timestamp: p.timestamp.toDate(),
        );
      }).toList(),
    );
  }

  Stream<List<PostcardEntity>> _convertSnapshotToEntity(
      Stream<QuerySnapshot<PostcardEntity>> snapshotStream) {
    return snapshotStream.map((QuerySnapshot<PostcardEntity> snapshot) {
      return snapshot.docs.map((e) => e.data()).toList();
    });
  }

  Stream<List<Postcard>> _processSnapshot(
      Stream<QuerySnapshot<PostcardEntity>> snapshotStream) {
    final postcardStream = _convertSnapshotToEntity(snapshotStream);
    return _getPostcardStream(postcardStream);
  }
}
