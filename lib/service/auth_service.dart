import 'dart:io';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:image_picker/image_picker.dart';
import 'package:stamped/data/entities/user.dart' as stamped;
import 'package:stamped/data/enums/image_type.dart';
import 'package:stamped/service/media_service.dart';
import 'package:stamped/service/user_service.dart';

class AuthService {
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

  final UserService _userService;
  final MediaService _mediaService;

  Stream<User?> get userStream => _firebaseAuth.userChanges();

  stamped.User _currentUser = stamped.User(
    id: '0',
    userName: 'Test User',
    profilePicture: '',
    lastLocation: 'Slovakia',
    isAdmin: false,
    googleAuthId: '0',
  );

  stamped.User get currentUser => _currentUser;

  AuthService(this._userService, this._mediaService);

  Future<UserCredential> signInWithCredential(
    AuthCredential credential,
  ) async =>
      await _firebaseAuth.signInWithCredential(credential);

  Future<bool> setUser() async {
    if (kDebugMode) {
      print('Running in debug mode');
      const mockUserAuthId = 'SGmjvaGbsPeH2XsqZYZ2FiKTWmL2';
      try {
        _currentUser = await _userService.updateUserLocation(mockUserAuthId);
      } catch (e) {
        print(e);
      }
      return true;
    } else if (kReleaseMode) {
      print('Running in release mode');
    } else if (kProfileMode) {
      print('Running in profile mode');
    }
    final firebaseUser = _firebaseAuth.currentUser!;

    try {
      _currentUser = await _userService.updateUserLocation(firebaseUser.uid);
    } catch (e) {
      _currentUser = await _userService.createFromFirebase(firebaseUser);
    }

    return true;
  }

  Future<void> singOut() async {
    await GoogleSignIn().signOut();
    FirebaseAuth.instance.signOut();
  }

  Future<void> changeDisplayName(String newName) async {
    final currentAuthUser = await userStream.first;
    currentAuthUser!.updateDisplayName(newName);

    final user = await _userService.findByGoogleId(currentAuthUser.uid);
    _userService.changeUserName(id: user.id, newName: newName);
  }

  Future<void> changeProfilePicture() async {
    final pickedFile =
        await ImagePicker().pickImage(source: ImageSource.gallery);

    if (pickedFile == null) {
      return;
    }

    final imageRef = await _mediaService.uploadImage(
      File(pickedFile.path),
      imageType: ImageType.profilePicture,
    );

    final imageUrl = await imageRef!.getDownloadURL();

    final currentAuthUser = await userStream.first;
    currentAuthUser!.updatePhotoURL(imageUrl);

    final user = await _userService.findByGoogleId(currentAuthUser.uid);
    _userService.changeProfilePicture(id: user.id, pictureUrl: imageUrl);
  }
}
