import 'package:stamped/data/entities/user.dart';

class Postcard {
  final String id;
  final String imagePath;
  final String stampPath;
  final String originLocation;
  final String destinationLocation;
  final String text;
  final User sender;
  final User? receiver;
  final DateTime timestamp;

  const Postcard({
    required this.id,
    required this.imagePath,
    required this.stampPath,
    required this.originLocation,
    required this.destinationLocation,
    required this.text,
    required this.sender,
    this.receiver,
    required this.timestamp,
  });
}
