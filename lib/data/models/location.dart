import 'package:geocoding/geocoding.dart';
import 'package:stamped/data/models/country.dart';

class Location {
  final double latitude;

  final double longitude;

  late final Country country;

  late final String place;

  Location({required this.latitude, required this.longitude}) {
    _fetchLocation();
  }

  Future<void> _fetchLocation() async {
    try {
      List<Placemark> placemarks =
          await placemarkFromCoordinates(latitude, longitude);
      if (placemarks.isNotEmpty) {
        final placemark = placemarks.first;
        final _country = placemark.country ?? "Unknown";

        this.country = _getCountryFlag(_country);
        this.place = placemark.name ?? "Unknown";
      } else {
        print("No placemarks found");
      }
    } catch (e) {
      print("Error: $e");
    }
  }

  Country _getCountryFlag(String country) {
    // query country flag based on country
    final flag = "";
    return Country(name: country, flag: flag);
  }
}
