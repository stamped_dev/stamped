// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'postcard_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PostcardEntity _$PostcardEntityFromJson(Map<String, dynamic> json) =>
    PostcardEntity(
      id: json['id'] as String,
      imagePath: json['imagePath'] as String,
      stampPath: json['stampPath'] as String,
      originLocation: json['originLocation'] as String,
      destinationLocation: json['destinationLocation'] as String,
      text: json['text'] as String,
      senderId: json['senderId'] as String,
      receiverId: json['receiverId'] as String?,
      timestamp: PostcardEntity._timestampFromJson(json['timestamp']),
    );

Map<String, dynamic> _$PostcardEntityToJson(PostcardEntity instance) =>
    <String, dynamic>{
      'id': instance.id,
      'imagePath': instance.imagePath,
      'stampPath': instance.stampPath,
      'originLocation': instance.originLocation,
      'destinationLocation': instance.destinationLocation,
      'text': instance.text,
      'senderId': instance.senderId,
      'receiverId': instance.receiverId,
      'timestamp': PostcardEntity._timestampToJson(instance.timestamp),
    };
