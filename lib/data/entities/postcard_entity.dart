import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:json_annotation/json_annotation.dart';

part 'postcard_entity.g.dart';

@JsonSerializable()
class PostcardEntity {
  final String id;
  final String imagePath;
  final String stampPath;
  final String originLocation;
  final String destinationLocation;
  final String text;
  final String senderId;
  final String? receiverId;

  @JsonKey(fromJson: _timestampFromJson, toJson: _timestampToJson)
  final Timestamp timestamp;

  const PostcardEntity({
    required this.id,
    required this.imagePath,
    required this.stampPath,
    required this.originLocation,
    required this.destinationLocation,
    required this.text,
    required this.senderId,
    this.receiverId,
    required this.timestamp
  });

  factory PostcardEntity.fromJson(Map<String, dynamic> json) => _$PostcardEntityFromJson(json);
  Map<String, dynamic> toJson() => _$PostcardEntityToJson(this);

  static Timestamp _timestampFromJson(dynamic json) {
    if (json is Timestamp) {
      return json;
    } else if (json is Map<String, dynamic>) {
      return Timestamp(json['seconds'] as int, json['nanoseconds'] as int);
    } else {
      throw Exception('Unknown timestamp format in json: $json');
    }
  }

  static dynamic _timestampToJson(Timestamp timestamp) {
    return timestamp.toDate();
  }
}
