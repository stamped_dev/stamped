// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

User _$UserFromJson(Map<String, dynamic> json) => User(
      id: json['id'] as String,
      googleAuthId: json['googleAuthId'] as String,
      userName: json['userName'] as String,
      profilePicture: json['profilePicture'] as String,
      lastLocation: json['lastLocation'] as String,
      isAdmin: json['isAdmin'] as bool,
    );

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      'id': instance.id,
      'googleAuthId': instance.googleAuthId,
      'userName': instance.userName,
      'profilePicture': instance.profilePicture,
      'lastLocation': instance.lastLocation,
      'isAdmin': instance.isAdmin,
    };
