import 'package:json_annotation/json_annotation.dart';

part 'user.g.dart';

@JsonSerializable()
class User {
  final String id;
  final String googleAuthId;
  final String userName;
  final String profilePicture;
  final String lastLocation;
  final bool isAdmin;

  const User({
    required this.id,
    required this.googleAuthId,
    required this.userName,
    required this.profilePicture,
    required this.lastLocation,
    required this.isAdmin,
  });

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);
  Map<String, dynamic> toJson() => _$UserToJson(this);
}
