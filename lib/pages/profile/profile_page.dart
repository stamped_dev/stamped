import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:stamped/common/service/ioc_container.dart';
import 'package:stamped/common/util/shared_ui_constants.dart';
import 'package:stamped/common/widget/page_template.dart';
import 'package:stamped/common/widget/stream_builder_handler.dart';
import 'package:stamped/service/auth_service.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  final _authService = get<AuthService>();

  late final TextEditingController _textEditingController;

  @override
  void initState() {
    super.initState();
    _textEditingController = TextEditingController();
  }

  @override
  void dispose() {
    _textEditingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return PageTemplate(
      title: 'Profile',
      actions: kDebugMode
          ? []
          : [
              IconButton(
                onPressed: () => _authService.singOut(),
                icon: Icon(Icons.logout),
              ),
            ],
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(
              height: 160,
              child: StreamBuilderHandler(
                stream: _authService.userStream,
                builder: (context, data) {
                  return Stack(
                    children: [
                      CircleAvatar(
                        backgroundImage:
                            CachedNetworkImageProvider(data!.photoURL!),
                        radius: 80,
                      ),
                      Positioned(
                        right: 0,
                        top: 0,
                        child: Container(
                          constraints: BoxConstraints(
                            minWidth: 10,
                            minHeight: 10,
                          ),
                          child: _buildEditButton(
                            () {
                              try {
                                _authService.changeProfilePicture();
                              } on Exception catch (e) {
                                if (context.mounted) {
                                  ScaffoldMessenger.of(context).showSnackBar(
                                    SnackBar(
                                      content: Text(
                                        e.toString(),
                                      ),
                                    ),
                                  );
                                }
                              }
                            },
                          ),
                        ),
                      ),
                    ],
                  );
                },
              ),
            ),
            SizedBox(
              height: STANDARD_GAP,
            ),
            StreamBuilderHandler(
              stream: _authService.userStream,
              builder: (context, snapshot) {
                return Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(
                      snapshot!.displayName ?? '',
                      style: TextStyle(fontSize: 20),
                    ),
                    SizedBox(
                      width: STANDARD_GAP * 0.3,
                    ),
                    _buildEditButton(
                      () async {
                        final newDisplayName =
                            await _showEditDisplayNameDialog(context);

                        if (newDisplayName == null || newDisplayName.isEmpty) {
                          return;
                        }

                        try {
                          _authService.changeDisplayName(newDisplayName);
                        } on Exception catch (e) {
                          if (context.mounted) {
                            ScaffoldMessenger.of(context).showSnackBar(
                              SnackBar(
                                content: Text(
                                  e.toString(),
                                ),
                              ),
                            );
                          }
                        }
                      },
                    ),
                  ],
                );
              },
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildEditButton(
    VoidCallback onPressed,
  ) =>
      CircleAvatar(
        backgroundColor: Colors.blue[100],
        child: IconButton(
          onPressed: onPressed,
          icon: Icon(Icons.edit),
        ),
      );

  Future<String?> _showEditDisplayNameDialog(BuildContext context) =>
      showDialog<String>(
        context: context,
        builder: (context) => AlertDialog(
          title: Text('Edit Display Name'),
          content: TextField(
            decoration: InputDecoration(hintText: 'Enter a new display name'),
            autofocus: true,
            controller: _textEditingController,
          ),
          actions: [
            TextButton(
              onPressed: () {
                _submitDisplayName();
              },
              child: Text('Submit'),
            ),
          ],
        ),
      );

  void _submitDisplayName() {
    Navigator.of(context).pop(_textEditingController.text);
    _textEditingController.clear();
  }
}
