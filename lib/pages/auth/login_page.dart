import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_signin_button/flutter_signin_button.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:stamped/common/service/ioc_container.dart';
import 'package:stamped/common/util/shared_ui_constants.dart';
import 'package:stamped/service/auth_service.dart';

class LoginPage extends StatelessWidget {
  final _authService = get<AuthService>();

  LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _buildContentWithBackground(),
    );
  }

  Container _buildContentWithBackground() {
    return Container(
      width: double.infinity,
      height: double.infinity,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('lib/assets/login-background.jpg'),
          fit: BoxFit.cover,
        ),
      ),
      alignment: Alignment.bottomCenter,
      padding: EdgeInsets.only(
        top: STANDARD_GAP * 10,
        bottom: STANDARD_GAP * 6,
      ),
      child: Center(
        child: _buildLoginContent(),
      ),
    );
  }

  Column _buildLoginContent() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisSize: MainAxisSize.min,
      children: [
        Text(
          'Welcome to Stamped!',
          style: TextStyle(
            fontSize: 30,
          ),
        ),
        Spacer(),
        Text(
          'Please sign in to continue',
          style: TextStyle(color: Colors.grey[600]),
        ),
        SizedBox(
          height: 15,
        ),
        _buildSignInWithGoogleButton(),
      ],
    );
  }

  Widget _buildSignInWithGoogleButton() {
    return SizedBox(
      height: 50,
      child: SignInButton(
        Buttons.Google,
        onPressed: () {
          _signInWithGoogle();
        },
        text: 'Sign In With Google',
      ),
    );
  }

  void _signInWithGoogle() async {
    GoogleSignInAccount? googleUser = await GoogleSignIn().signIn();

    GoogleSignInAuthentication? googleAuth = await googleUser?.authentication;

    AuthCredential credential = GoogleAuthProvider.credential(
      accessToken: googleAuth?.accessToken,
      idToken: googleAuth?.idToken,
    );

    await _authService.signInWithCredential(credential);
  }
}
