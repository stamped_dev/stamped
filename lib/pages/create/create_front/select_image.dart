import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';
import 'package:stamped/common/widget/page_template.dart';
import 'package:stamped/pages/create/create_back/create_back_page.dart';
import 'package:stamped/pages/create/notifiers/create_postcard_notifier.dart';

class SelectImage extends StatelessWidget {
  const SelectImage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final createPostcardNotifier = context.watch<CreatePostcardNotifier>();

    return PageTemplate(
      title: 'Create a Postcard',
      actions: [
        IconButton(
          onPressed: () {
            if (createPostcardNotifier.selectedImage == null) {
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  content: Text(
                    'Please select an image first.',
                  ),
                ),
              );

              return;
            }

            Navigator.of(context).push(
              MaterialPageRoute(
                builder: (context) => CreateBackPage(),
              ),
            );
          },
          icon: Icon(Icons.navigate_next),
        ),
      ],
      child: Align(
        alignment: Alignment.bottomCenter,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Spacer(),
            createPostcardNotifier.selectedImage == null
                ? Text('No Image Selected')
                : Image.file(
                    createPostcardNotifier.selectedImage!,
                    width: double.infinity,
                  ),
            Spacer(),
            _buildPickImageButton(
              createPostcardNotifier,
              source: ImageSource.gallery,
            ),
            _buildPickImageButton(
              createPostcardNotifier,
              source: ImageSource.camera,
            ),
          ],
        ),
      ),
    );
  }

  ElevatedButton _buildPickImageButton(
    CreatePostcardNotifier createPostcardNotifier, {
    required ImageSource source,
  }) {
    return ElevatedButton.icon(
      onPressed: () => createPostcardNotifier.pickImage(source),
      icon: Icon(Icons.photo_library),
      label: Text(source == ImageSource.gallery
          ? 'Pick Image from Gallery'
          : 'Take Image from Camera'),
    );
  }
}
