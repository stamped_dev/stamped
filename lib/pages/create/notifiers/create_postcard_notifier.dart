import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:country_picker/country_picker.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:stamped/common/service/ioc_container.dart';
import 'package:stamped/data/entities/postcard_entity.dart';
import 'package:stamped/data/enums/image_type.dart';
import 'package:stamped/service/auth_service.dart';
import 'package:stamped/service/media_service.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:stamped/service/postcard_service.dart';
import 'package:stamped/service/user_service.dart';

class CreatePostcardNotifier extends ChangeNotifier {
  final _mediaService = get<MediaService>();
  final _userService = get<UserService>();
  final _postcardService = get<PostcardService>();
  final _authService = get<AuthService>();

  File? _postcardImage;
  String _postcardImageDownloadUrl = '';
  String text = '';
  Country? _destination;

  File? get selectedImage => _postcardImage;
  Future<String> get location async =>
      await _userService.getCurrentDeviceLocation();
  Country? get destination => _destination;

  set destination(Country? destination) {
    _destination = destination;
    notifyListeners();
  }

  Future<void> pickImage(ImageSource source) async {
    final pickedFile = await ImagePicker().pickImage(source: source);
    if (pickedFile != null) {
      _postcardImage = File(pickedFile.path);
      notifyListeners();

      final croppedFile = await cropImage();
      if (croppedFile != null) {
        _postcardImage = File(croppedFile.path);
        notifyListeners();
      }
    }
  }

  Future<CroppedFile?> cropImage() async {
    CroppedFile? croppedFile = await ImageCropper().cropImage(
      sourcePath: _postcardImage!.path,
      uiSettings: [
        AndroidUiSettings(
          toolbarTitle: 'Crop Image',
          toolbarColor: Colors.white,
          initAspectRatio: CropAspectRatioPreset.square,
          lockAspectRatio: true,
          hideBottomControls: true,
          backgroundColor: Colors.white,
          activeControlsWidgetColor: Colors.grey,
        ),
        IOSUiSettings(
          title: 'Cropper',
        ),
      ],
    );

    return croppedFile;
  }

  Future<void> uploadPostcardImage() async {
    try {
      final imageRef = await _mediaService.uploadImage(
        _postcardImage!,
        imageType: ImageType.postcard,
      );

      _postcardImageDownloadUrl = await imageRef!.getDownloadURL();
    } on FirebaseException catch (_) {
      rethrow;
    }
  }

  Future<void> createPostcard() async {
    await uploadPostcardImage();

    final allCountryStamps = await _mediaService.getStampUrls(
      await location.then(
        (value) => value.toLowerCase(),
      ),
    );
    allCountryStamps.shuffle();
    final stampUrl = await allCountryStamps.first;

    _postcardService.create(
      PostcardEntity(
        id: '0',
        imagePath: _postcardImageDownloadUrl,
        stampPath: stampUrl,
        originLocation: await location,
        destinationLocation: _destination!.name,
        text: text,
        senderId: _authService.currentUser.id,
        receiverId: '',
        timestamp: Timestamp.now(),
      ),
    );

    _clear();
    notifyListeners();
  }

  void _clear() {
    _postcardImage = null;
    _postcardImageDownloadUrl = '';
    text = '';
    _destination = null;
  }
}
