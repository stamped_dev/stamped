import 'package:country_picker/country_picker.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:stamped/bottom_nav_bar/widget/bottom_nav_bar.dart';
import 'package:stamped/common/service/ioc_container.dart';
import 'package:stamped/common/util/shared_ui_constants.dart';
import 'package:stamped/common/widget/future_builder_handler.dart';
import 'package:stamped/common/widget/page_template.dart';
import 'package:stamped/pages/create/notifiers/create_postcard_notifier.dart';
import 'package:stamped/service/auth_service.dart';

const FONT_SIZE = 18.0;

class CreateBackPage extends StatefulWidget {
  const CreateBackPage({Key? key}) : super(key: key);

  @override
  State<CreateBackPage> createState() => _CreateBackPageState();
}

class _CreateBackPageState extends State<CreateBackPage> {
  late final TextEditingController _textEditingController;

  @override
  void initState() {
    super.initState();
    _textEditingController = TextEditingController();
  }

  @override
  void dispose() {
    _textEditingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final authService = get<AuthService>();
    final createPostcardNotifier = context.watch<CreatePostcardNotifier>();

    return PageTemplate(
      title: 'Create a Postcard',
      actions: [
        _buildSubmitButton(createPostcardNotifier, context),
      ],
      child: _buildPostcardBackground(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _buildTextField(),
            Spacer(),
            Divider(
              color: Colors.black87,
            ),
            Spacer(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                _buildFadedText('Greetings from'),
                _buildFadedText('To:')
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                // _buildBoldText(createPostcardNotifier),
                FutureBuilderHandler(
                  future: createPostcardNotifier.location,
                  builder: (context, data) => _buildBoldText(data),
                ),
                _buildDestinationSelector(context, createPostcardNotifier),
              ],
            ),
            Spacer(),
            _buildFooter(
              createPostcardNotifier,
              authService,
            ),
            Spacer(),
          ],
        ),
      ),
    );
  }

  IconButton _buildSubmitButton(
    CreatePostcardNotifier createPostcardNotifier,
    BuildContext context,
  ) =>
      IconButton(
        onPressed: () {
          if (_textEditingController.text == '') {
            ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(
                content: Text(
                  'Please enter a message.',
                ),
              ),
            );

            return;
          }

          if (createPostcardNotifier.destination == null) {
            ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(
                content: Text(
                  'Please select a destination country.',
                ),
              ),
            );

            return;
          }

          createPostcardNotifier.text = _textEditingController.text;

          try {
            createPostcardNotifier.createPostcard();
          } on Exception catch (e) {
            ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(
                content: Text(
                  e.toString(),
                ),
              ),
            );
          }

          Navigator.pushReplacement(
            context,
            MaterialPageRoute(
              builder: (context) => BottomNavBar(),
            ),
          );
        },
        icon: Icon(Icons.check),
      );

  Widget _buildPostcardBackground({required Widget child}) => Container(
        decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(
            color: Colors.grey,
            width: 2,
          ),
        ),
        padding: EdgeInsets.all(STANDARD_GAP),
        height: double.infinity,
        child: child,
      );

  Widget _buildDestinationSelector(
    BuildContext context,
    CreatePostcardNotifier createPostcardNotifier,
  ) {
    final isNull = createPostcardNotifier.destination == null;

    return InkWell(
      onTap: () {
        showCountryPicker(
          context: context,
          onSelect: (Country country) {
            createPostcardNotifier.destination = country;
          },
        );
      },
      child: isNull
          ? Icon(
              Icons.cancel_presentation_sharp,
              color: Colors.black45,
            )
          : Text(
              createPostcardNotifier.destination?.flagEmoji ??
                  'Select a destination',
              style: TextStyle(
                fontSize: FONT_SIZE,
              ),
            ),
    );
  }

  Widget _buildTextField() => TextField(
        maxLength: 220,
        maxLines: 12,
        style: TextStyle(
          fontSize: FONT_SIZE,
        ),
        decoration: InputDecoration(
          hintText: 'Type a message',
          border: InputBorder.none,
        ),
        controller: _textEditingController,
      );

  Widget _buildFadedText(String text) => Text(
        text,
        style: TextStyle(
          color: Colors.grey,
          fontSize: FONT_SIZE,
          fontWeight: FontWeight.w600,
        ),
      );

  Widget _buildBoldText(String text) => Text(
        text,
        style: TextStyle(
          color: Colors.black,
          fontSize: FONT_SIZE,
          fontWeight: FontWeight.w800,
        ),
      );

  Widget _buildFooter(
    CreatePostcardNotifier notifier,
    AuthService authService,
  ) =>
      Row(
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        children: [
          Container(
            height: 100,
            width: 100,
            decoration: BoxDecoration(
              border: Border.all(
                color: Colors.grey[350]!,
                width: 2,
              ),
            ),
          ),
          _buildSignature(authService),
        ],
      );

  Widget _buildSignature(AuthService authService) => Expanded(
        child: Text(
          textAlign: TextAlign.right,
          authService.currentUser.userName,
          style: TextStyle(
            fontSize: FONT_SIZE,
            fontWeight: FontWeight.w800,
          ),
        ),
      );
}
