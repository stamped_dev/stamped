import 'package:flutter/material.dart';
import 'package:stamped/common/service/ioc_container.dart';
import 'package:stamped/common/util/shared_ui_constants.dart';
import 'package:stamped/common/widget/page_template.dart';
import 'package:stamped/common/widget/stream_builder_handler.dart';
import 'package:stamped/data/models/postcard.dart';
import 'package:stamped/pages/collections/postcard_tile.dart';
import 'package:stamped/pages/collections/stamp_tile.dart';
import 'package:stamped/service/postcard_service.dart';

class HomePage extends StatelessWidget {
  HomePage({Key? key}) : super(key: key);

  final postcardService = get<PostcardService>();

  @override
  Widget build(BuildContext context) {
    return PageTemplate(
      title: 'Stamped!',
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          children: [
            _buildHeading('Postcards in Your Area'),
            _buildScrollList<Postcard>(context, 250,
                postcardService.availablePostcardStream, PostcardTile.fromData),
            SizedBox(
              height: STANDARD_GAP,
            ),
            _buildHeading('Recently Obtained Stamps'),
            _buildScrollList<String>(context, 220,
                postcardService.recentStampStream, StampTile.fromData),
            SizedBox(
              height: STANDARD_GAP,
            ),
            _buildHeading('Your Last Postcards'),
            _buildScrollList<Postcard>(context, 250,
                postcardService.recentPostcardStream, PostcardTile.fromData),
          ],
        ),
      ),
    );
  }

  Widget _buildHeading(
    String value,
  ) =>
      Text(
        value,
        style: TextStyle(
          fontSize: 18,
          fontWeight: FontWeight.bold,
        ),
      );

  Widget _buildScrollList<T>(BuildContext context, double height,
      Stream<Iterable<T>> stream, Widget Function(T) builder) {
    return Container(
      height: height,
      width: MediaQuery.of(context).size.width,
      child: StreamBuilderHandler(
          stream: stream,
          builder: (BuildContext context, data) {
            return ListView.builder(
              itemBuilder: (context, index) => builder(data.elementAt(index)),
              itemCount: data.length,
              scrollDirection: Axis.horizontal,
            );
          }),
    );
  }
}
