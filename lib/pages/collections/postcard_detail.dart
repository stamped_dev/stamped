import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:stamped/common/service/ioc_container.dart';
import 'package:stamped/common/widget/cached_image_builder.dart';
import 'package:stamped/common/widget/page_template.dart';
import 'package:stamped/data/entities/postcard_entity.dart';
import 'package:stamped/data/models/postcard.dart';
import 'package:stamped/pages/collections/postcard_detial_back.dart';
import 'package:stamped/service/auth_service.dart';
import 'package:stamped/service/postcard_service.dart';

class PostcardDetail extends StatelessWidget {
  final Postcard postcard;
  final _postcardService = get<PostcardService>();
  final _authService = get<AuthService>();

  PostcardDetail({super.key, required this.postcard});

  @override
  Widget build(BuildContext context) {
    return PageTemplate(
      title: '',
      padding: 0,
      actions: _getActions(context),
      child: GestureDetector(
          onTap: () {
            Navigator.of(context).push(_createRoute());
          },
          child: CachedImageBuilder(
            url: postcard.imagePath,
          )),
    );
  }

  Route _createRoute() {
    return PageRouteBuilder(
      pageBuilder: (context, animation, secondaryAnimation) =>
          PostcardDetailBack(postcard: postcard),
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        const begin = Offset(1.0, 0);
        const end = Offset.zero;
        var curve = Curves.easeInOutCubic;
        var tween =
            Tween(begin: begin, end: end).chain(CurveTween(curve: curve));
        final offsetAnimation = animation.drive(tween);
        return SlideTransition(
          position: offsetAnimation,
          child: child,
        );
      },
    );
  }

  List<Widget> _getActions(BuildContext context) {
    return postcard.receiver == null
        ? [
            IconButton(
              icon: const Icon(Icons.save),
              tooltip: 'Save Postcard',
              onPressed: () async {
                final updatedEntity = _createUpdatedEntity();
                try {
                  await _postcardService.updatePostcardReceiver(updatedEntity);
                  ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                      content: Text('Postcard successfully saved')));
                } catch (e) {
                  ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                      content: Text('Postcard save failed')));
                }

              },
            )
          ]
        : [];
  }

  PostcardEntity _createUpdatedEntity() {
    return PostcardEntity(
      id: postcard.id,
      imagePath: postcard.imagePath,
      stampPath: postcard.stampPath,
      originLocation: postcard.originLocation,
      destinationLocation: postcard.destinationLocation,
      text: postcard.text,
      senderId: postcard.sender.id,
      receiverId: _authService.currentUser.id,
      timestamp: Timestamp.fromDate(postcard.timestamp),
    );
  }
}
