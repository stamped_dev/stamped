import 'package:flutter/material.dart';
import 'package:stamped/common/widget/page_template.dart';
import 'package:stamped/data/models/postcard.dart';
import 'package:stamped/pages/collections/stamp_tile.dart';

class PostcardDetailBack extends StatelessWidget {
  final Postcard postcard;

  const PostcardDetailBack({super.key, required this.postcard});

  static const _white = Colors.white;
  static const _grey = Color(0x40656565);
  static const _black = Colors.black;
  static const _textGrey = Colors.grey;
  static const _textBlack = Colors.black;
  static const _partitionsPadding = 16.0;

  @override
  Widget build(BuildContext context) {
    return PageTemplate(
      title: '',
      child: GestureDetector(
        onTap: () => Navigator.pop(context),
        child: Container(
          decoration: BoxDecoration(
            color: _white,
            border: Border.all(
              color: _grey,
              width: 2,
            ),
          ),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              _postcardText(),
              Divider(color: _black, indent: 10, endIndent: 10),
              Padding(
                padding: const EdgeInsets.all(_partitionsPadding),
                child: Container(
                  height: 200,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      _greetingsText(),
                      _locationText(),
                      _stampRow(),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _greetingsText() {
    return Text(
      'Greetings from',
      style: TextStyle(
        color: _textGrey,
        fontSize: 18,
        fontWeight: FontWeight.w600,
      ),
    );
  }

  Widget _locationText() {
    return Text(
      postcard.originLocation,
      style: TextStyle(
        color: _textBlack,
        fontSize: 18,
        fontWeight: FontWeight.w800,
      ),
    );
  }

  Widget _stampRow() {
    return Expanded(
      child: Padding(
        padding: EdgeInsets.all(_partitionsPadding),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            StampTile(url: postcard.stampPath, height: 100, width: 100, padding: 0,),
            _nameText(),
          ],
        ),
      ),
    );
  }

  Widget _postcardText() {
    return Expanded(
        child: Padding(
      padding: const EdgeInsets.all(_partitionsPadding),
      child: Text(
        textAlign: TextAlign.justify,
        postcard.text,
        style: TextStyle(
          color: _textBlack,
          fontSize: 18,
          fontWeight: FontWeight.w500,
        ),
      ),
    ));
  }

  Widget _nameText() {
    return Expanded(
      child: Center(
        child: Padding(
          padding: const EdgeInsets.only(left: _partitionsPadding),
          child: Text(
            textAlign: TextAlign.right,
            postcard.sender.userName,
            style: TextStyle(
              color: _textBlack,
              fontSize: 18,
              fontWeight: FontWeight.w800,
            ),
          ),
        ),
      ),
    );
  }
}
