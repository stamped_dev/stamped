import 'package:flutter/material.dart';
import 'package:stamped/common/widget/cached_image_builder.dart';
import 'package:stamped/pages/collections/stamp_detail.dart';

class StampTile extends StatelessWidget {
  final String url;
  final double? height;
  final double? width;
  final double padding;

  const StampTile(
      {super.key,
      required this.url,
      this.height,
      this.width,
      this.padding = 8.0});

  static StampTile fromData(String data) {
    return StampTile(url: data);
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(padding),
      child: GestureDetector(
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => StampDetail(url: url)),
          );
        },
        child: CachedImageBuilder(
          url: url,
          fit: BoxFit.contain,
          width: width ?? MediaQuery.of(context).size.width / 2 - 40,
          height: height,
        ),
      ),
    );
  }
}
