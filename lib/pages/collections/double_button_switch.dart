import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:stamped/data/models/collection_type.dart';
import 'package:stamped/pages/collections/notifiers/collection_notifier.dart';

class DoubleButtonSwitch extends StatelessWidget {
  DoubleButtonSwitch({super.key});

  static const _buttonOffText = Colors.black;
  static const _buttonOnText = Colors.white;
  static const _buttonOffBackground = Colors.white;
  static const _buttonOnBackground = Colors.blue;

  @override
  Widget build(BuildContext context) {
    final collectionsNotifier = context.watch<CollectionNotifier>();
    return GestureDetector(
      onTap: () => collectionsNotifier.changeCollection(),
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Expanded(
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.horizontal(
                    left: Radius.circular(11),
                    right: Radius.zero,
                  ),
                  color: collectionsNotifier.collectionType ==
                          CollectionType.postcards
                      ? _buttonOnBackground
                      : _buttonOffBackground,
                ),
                child: Center(
                  child: Text(
                    'Postcards',
                    style: TextStyle(
                      color: collectionsNotifier.collectionType ==
                              CollectionType.postcards
                          ? _buttonOnText
                          : _buttonOffText,
                      fontSize: 22,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
              ),
            ),
            Expanded(
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.horizontal(
                    right: Radius.circular(11),
                    left: Radius.zero,
                  ),
                  color: collectionsNotifier.collectionType ==
                          CollectionType.stamps
                      ? _buttonOnBackground
                      : _buttonOffBackground,
                ),
                child: Center(
                  child: Text(
                    'Stamps',
                    style: TextStyle(
                      color: collectionsNotifier.collectionType ==
                              CollectionType.stamps
                          ? _buttonOnText
                          : _buttonOffText,
                      fontSize: 22,
                      fontWeight: FontWeight.w500,
                    ),
                    // ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
