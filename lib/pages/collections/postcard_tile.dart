import 'package:flutter/material.dart';
import 'package:stamped/common/service/ioc_container.dart';
import 'package:stamped/common/widget/cached_image_builder.dart';
import 'package:stamped/common/widget/future_builder_handler.dart';
import 'package:stamped/data/models/postcard.dart';
import 'package:stamped/pages/collections/postcard_detail.dart';
import 'package:stamped/service/media_service.dart';

class PostcardTile extends StatelessWidget {
  final Postcard postcard;

  const PostcardTile({super.key, required this.postcard});

  static const _tileBackground = Colors.white;

  static PostcardTile fromData(Postcard data) {
    return PostcardTile(postcard: data);
  }

  @override
  Widget build(BuildContext context) {
    final MediaService mediaService = get<MediaService>();

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: GestureDetector(
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => PostcardDetail(postcard: postcard)),
          );
        },
        child: Container(
          decoration: BoxDecoration(
              color: _tileBackground,
              borderRadius: BorderRadius.all(Radius.circular(10))),
          width: MediaQuery.of(context).size.width / 2 - 40,
          padding: EdgeInsets.all(10),
          child: Column(
            children: [
              _imageBuilder(),
              SizedBox(
                height: 8,
              ),
              _senderDetail(context, mediaService)
            ],
          ),
        ),
      ),
    );
  }

  Expanded _imageBuilder() {
    return Expanded(
      child: CachedImageBuilder(
        url: postcard.imagePath,
      ),
    );
  }

  Widget _senderDetail(
    BuildContext context,
    MediaService mediaService,
  ) {
    return Row(
      children: [
        Text(postcard.sender.userName.length > 13
            ? '${postcard.sender.userName.substring(0, 13)}..'
            : postcard.sender.userName),
        Spacer(),
        Text(
          mediaService.getFlagBitcode(postcard.originLocation),
          style:
              TextStyle(fontSize: MediaQuery.of(context).textScaleFactor * 20),
        ),
      ],
    );
  }
}
