import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:stamped/common/service/ioc_container.dart';
import 'package:stamped/common/widget/stream_builder_handler.dart';
import 'package:stamped/data/models/collection_type.dart';
import 'package:stamped/pages/collections/notifiers/collection_notifier.dart';
import 'package:stamped/pages/collections/postcard_tile.dart';
import 'package:stamped/pages/collections/stamp_tile.dart';
import 'package:stamped/service/postcard_service.dart';

class PostcardCollection extends StatelessWidget {
  const PostcardCollection({super.key});

  @override
  Widget build(BuildContext context) {
    final collectionNotifier = context.watch<CollectionNotifier>();
    return collectionNotifier.collectionType == CollectionType.postcards
        ? _postcardCollection(collectionNotifier)
        : _stampCollection();
  }

  Widget _postcardCollection(CollectionNotifier notifier) {
    return Expanded(
      child: StreamBuilderHandler(
        stream: notifier.filteredPostcards,
        builder: (context, postcards) {
          return GridView.count(
            childAspectRatio: 0.7,
            crossAxisCount: 2,
            children: postcards.map<PostcardTile>((postcard) {
              return PostcardTile(
                key: ValueKey(postcard.id),
                postcard: postcard,
              );
            }).toList(),
          );
        },
      ),
    );
  }

  Widget _stampCollection() {
    final postcardService = get<PostcardService>();
    return Expanded(
      child: StreamBuilderHandler(
        stream: postcardService.stampStream,
        builder: (context, stamps) {
          return GridView.count(
            childAspectRatio: 1,
            crossAxisCount: 2,
            children: stamps.map<StampTile>((stampUrl) {
              return StampTile(url: stampUrl, padding: 0,);
            }).toList(),
          );
        },
      ),
    );
  }
}
