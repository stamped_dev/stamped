import 'package:flutter/cupertino.dart';
import 'package:stamped/common/service/ioc_container.dart';
import 'package:stamped/data/models/collection_type.dart';
import 'package:stamped/data/models/postcard.dart';
import 'package:stamped/pages/collections/notifiers/collection_filters.dart';
import 'package:stamped/service/postcard_service.dart';

class CollectionNotifier extends ChangeNotifier {
  final _filters = CollectionFilters();
  final _postcardService = get<PostcardService>();

  CollectionType get collectionType => _filters.type;

  String get text => _filters.text;

  Stream<List<Postcard>> get filteredPostcards {
    return _postcardService.postcardStream.map((postcards) {
      return postcards.where((postcard) {
        if (!postcard.sender.userName.contains(_filters.text)) return false;
        return true;
      }).toList();
    });
  }

  void changeCollection() {
    if (_filters.type == CollectionType.postcards) {
      _filters.type = CollectionType.stamps;
    } else {
      _filters.type = CollectionType.postcards;
    }
    notifyListeners();
  }

  void setTextSearch(String text) {
    _filters.text = text;
    notifyListeners();
  }
}
