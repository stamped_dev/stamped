import 'package:stamped/data/models/collection_type.dart';

class CollectionFilters {
  var type = CollectionType.postcards;
  var text = '';
}