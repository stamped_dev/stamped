import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:stamped/common/widget/page_template.dart';
import 'package:stamped/pages/collections/double_button_switch.dart';
import 'package:stamped/pages/collections/notifiers/collection_notifier.dart';
import 'package:stamped/pages/collections/postcard_collection.dart';
import 'package:stamped/pages/collections/postcard_filter.dart';

class CollectionsPage extends StatefulWidget {
  const CollectionsPage({super.key});

  @override
  State<CollectionsPage> createState() => _CollectionsPageState();
}

class _CollectionsPageState extends State<CollectionsPage> {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (BuildContext context) => CollectionNotifier(),
      child: PageTemplate(
        title: 'Collections',
        child: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              DoubleButtonSwitch(),
              PostcardFilter(),
              PostcardCollection(),
            ],
          ),
        ),
      ),
    );
  }
}
