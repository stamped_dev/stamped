import 'package:flutter/material.dart';
import 'package:stamped/pages/collections/filter/filter_option.dart';

class FilterDialog extends StatelessWidget {
  const FilterDialog({super.key});

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text("Filter"),
      content: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Form(
          child: Column(
            children: [
              FilterOption(),
              FilterOption(),
            ],
          ),
        ),
      ),
      actions: [
        ElevatedButton(
            child: Text("Submit"),
            onPressed: () {
              //TODO: add some submit here
            })
      ],
    );
  }
}
