import 'package:flutter/material.dart';
import 'package:stamped/common/widget/cached_image_builder.dart';
import 'package:stamped/common/widget/page_template.dart';

class StampDetail extends StatelessWidget {
  final _url;

  StampDetail({super.key, required url}) : this._url = url;

  @override
  Widget build(BuildContext context) {
    return PageTemplate(
      title: '',
      padding: 0,
      child: GestureDetector(
          onTap: () => Navigator.pop(context),
          child: CachedImageBuilder(url: _url, fit: BoxFit.contain),
      ),
    );
  }
}
