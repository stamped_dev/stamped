import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:stamped/pages/collections/notifiers/collection_notifier.dart';

class PostcardFilter extends StatelessWidget {
  const PostcardFilter({super.key});

  static const _searchbarBackground = Colors.white;

  @override
  Widget build(BuildContext context) {
    var inputText = '';
    final collectionNotifier = context.watch<CollectionNotifier>();
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Expanded(
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(
                  Radius.circular(10),
                ),
                color: _searchbarBackground,
              ),
              child: TextField(
                controller: TextEditingController(),
                onChanged: (text) => inputText = text,
                onSubmitted: (_) => collectionNotifier.setTextSearch(inputText),
                decoration: InputDecoration(
                  hintText: "Search",
                  icon: Icon(Icons.search),
                  border: InputBorder.none,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
